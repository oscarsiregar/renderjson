import * as React from 'react'
import  * as ReactDOMServer from 'react-dom/server';
import {DocumentRenderer} from '@keystone-6/document-renderer';

export function renderDocument(document: any){
    return ReactDOMServer.renderToStaticMarkup(
        <DocumentRenderer document={document}/>
    )
}